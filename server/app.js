var express = require("express");
var app = express();

console.log(__dirname);
const NODE_PORT = process.env.PORT || 4000;
console.log(NODE_PORT);
app.use(express.static(__dirname + "/../client/"));


app.use((req,res)=>{
    var y = 6;
    try{
        console.log("Y : " + y);
        res.send(`<h1> Oopps wrong please ${y}</h1>`);
    }catch(error){
        console.log(error);
        res.status(500).send("ERROR");
    } 
});

app.listen(NODE_PORT, ()=>{
    console.log(`Web App started at ${NODE_PORT}`);
});