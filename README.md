# How to commit my codes #
* git add .
* git commit -m "<message>"
* git push origin master -u

# SETUP #

This is everyday setup steps.

* git init
* git remote add origin <git url>
* npm init (answer all the questions , enter the entry point accordingly)
* mkdir server directory
* create a app.js under the server directory
* create .gitignore to ignore directories/files
* npm install express --save